<?php
/**
 * @file
 * showoff_video.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function showoff_video_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-video-body'
  $field_instances['node-video-body'] = array(
    'bundle' => 'video',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'display_summary' => 0,
      'text_processing' => '1',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => '20',
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => '1',
    ),
  );

  // Exported field_instance: 'node-video-field_date'
  $field_instances['node-video-field_date'] = array(
    'bundle' => 'video',
    'deleted' => '0',
    'description' => 'The date on which this video will initially be played in all corresponding feeds.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_date',
    'label' => 'Date',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => '15',
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => '3',
    ),
  );

  // Exported field_instance: 'node-video-field_duration'
  $field_instances['node-video-field_duration'] = array(
    'bundle' => 'video',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => 'The duration to show the video. This <em>MUST</em> be at least the length of the video, otherwise the video will be cut off before finishing playback!',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_duration',
    'label' => 'Duration',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => '5',
    ),
  );

  // Exported field_instance: 'node-video-field_feed'
  $field_instances['node-video-field_feed'] = array(
    'bundle' => 'video',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => 'The feeds in which to play the video.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_feed',
    'label' => 'Feeds',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => '7',
    ),
  );

  // Exported field_instance: 'node-video-field_video'
  $field_instances['node-video-field_video'] = array(
    'bundle' => 'video',
    'deleted' => '0',
    'description' => 'The video file to be played.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_video',
    'label' => 'Video',
    'required' => 1,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'videos',
      'file_extensions' => 'mov mpg mp4 ogg',
      'max_filesize' => '32 MB',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => '2',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Date');
  t('Description');
  t('Duration');
  t('Feeds');
  t('The date on which this video will initially be played in all corresponding feeds.');
  t('The duration to show the video. This <em>MUST</em> be at least the length of the video, otherwise the video will be cut off before finishing playback!');
  t('The feeds in which to play the video.');
  t('The video file to be played.');
  t('Video');

  return $field_instances;
}
